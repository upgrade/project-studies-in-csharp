﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Cadastros;
using Modelo.Tabelas;
using Persistencia.Contexts;

namespace Persistencia.DAL.Cadastros
{
    public class FabricanteDAL
    {
        private EFContext context = new EFContext();

        public IQueryable<Fabricante> ObterFabricanteClassificadoPorNome()
        {
            return context.Fabricantes.OrderBy(f => f.Nome);
        }

    }
}
