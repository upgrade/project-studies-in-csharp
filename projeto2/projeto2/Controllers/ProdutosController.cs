﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelo.Tabelas;
using Modelo.Cadastros;
using projeto2.Contexts;
using System.Data.Entity;
using System.Net;
using Servico.Cadastros;
using Servico.Tabelas;

namespace projeto2.Controllers
{
    public class ProdutosController : Controller
    {
        private ProdutoServico produtoServico = new ProdutoServico();
        private CategoriaServico categoriaServico = new CategoriaServico();
        private FabrivanteServico fabricanteServico = new FabrivanteServico();

        // GET: Produtos
        public ActionResult Index()
        {
            return View(produtoServico.ObterProdutosClassificadosPorNome());
        }

        private ActionResult ObterVisaoProdutoPorId(long? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = produtoServico.ObterProdutoPorId((long) id);
            if(produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);

        }

        private void PopularViewBag(Produto produto = null)
        {
            if(produto == null)
            {
                ViewBag.CategoriaId = new SelectList(categoriaServico.ObtercategoriaClassificadasPorNome(), "CategoriaId", "Nome");
                ViewBag.FabricanteId = new SelectList(fabricanteServico.ObterFabricanteClassificadosPorNome(), "FabricanteId", "Nome");
            }
            else
            {
                ViewBag.CategoriaId = new SelectList(categoriaServico.ObtercategoriaClassificadasPorNome(), "CategoriaId", "Nome", produto.CategoriaId);
                ViewBag.FabricanteId = new SelectList(fabricanteServico.ObterFabricanteClassificadosPorNome(), "FabricanteId", "Nome", produto.FabricanteId);

            }
        }

        // GET: Produtos/Details/5
        public ActionResult Details(long? id)
        {
            PopularViewBag(produtoServico.ObterProdutoPorId((long) id));
            return ObterVisaoProdutoPorId(id);
        }

        // GET: Produtos/Create
        public ActionResult Create()
        {
            PopularViewBag();
            return View();
        }

        private ActionResult GravarProduto(Produto produto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    produtoServico.GravarProdutoi(produto);
                    return RedirectToAction("Index");
                }
                return View(produto);
            }
            catch
            {
                return View(produto);
            }
        }

        // POST: Produtos/Create
        [HttpPost]
        public ActionResult Create(Produto produto)
        {
            /*try
            {
                // TODO: Add insert logic here
                // context.Produtos.Add(produto);
                //context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(produto);
            }*/
            return GravarProduto(produto);
        }

        // GET: Produtos/Edit/5
        public ActionResult Edit(long? id)
        {
            /* if(id == null)
             {
                 return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             }
             Produto produto = context.Produtos.Find(id);
             if(produto == null)
             {
                 return HttpNotFound();
             }
             ViewBag.CategoriaId = new SelectList(context.Categorias.OrderBy(b => b.Nome), "CategoriaId", "Nome", produto.CategoriaId);
             ViewBag.FabricanteId = new SelectList(context.Fabricantes.OrderBy(b => b.Nome), "FabricanteId", "Nome", produto.FabricanteId);
             return View(produto);*/
            PopularViewBag(produtoServico.ObterProdutoPorId((long)id));
            return ObterVisaoProdutoPorId(id);
        }

        // POST: Produtos/Edit/5
        [HttpPost]
        public ActionResult Edit(Produto produto)
        {
            /* try
             {
                 // TODO: Add update logic here
                 if (ModelState.IsValid)
                 {
                     context.Entry(produto).State = EntityState.Modified;
                     context.SaveChanges();
                     return RedirectToAction("Index");
                 }

                 return View(produto);
             }
             catch
             {
                 return View(produto);
             }*/
            return GravarProduto(produto);
        }

        // GET: Produtos/Delete/5
        public ActionResult Delete(long? id)
        {
            return ObterVisaoProdutoPorId(id);
        }

        // POST: Produtos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            /* try
             {
                 // TODO: Add delete logic here
                 Produto produto = context.Produtos.Find(id);
                 context.Produtos.Remove(produto);
                 context.SaveChanges();
                 TempData["Message"] = "Produto " + produto.Nome.ToUpper() + " for removido";
                 return RedirectToAction("Index");
             }
             catch
             {
                 return View();
             }*/
            try
            {
                Produto produto = produtoServico.EliminarProdutoId(id);
                TempData["Message"] = "Produto " + produto.Nome.ToUpper()
                + " foi removido";
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
