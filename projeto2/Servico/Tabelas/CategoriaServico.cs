﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Cadastros;
using Modelo.Tabelas;
using Persistencia.DAL.Tabelas;

namespace Servico.Tabelas
{
    public class CategoriaServico
    {
        public CategoriaDAL categoriaDAL = new CategoriaDAL();

        public IQueryable<Categoria> ObtercategoriaClassificadasPorNome()
        {
            return categoriaDAL.ObterCategoriasClassificadasPorNome();
        }
    }
}
