﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Cadastros;
using Modelo.Tabelas;
using Persistencia.DAL.Cadastros;

namespace Servico.Cadastros
{
    public class FabrivanteServico
    {
        private FabricanteDAL fabricanteDAL = new FabricanteDAL();

        public IQueryable<Fabricante> ObterFabricanteClassificadosPorNome()
        {
            return fabricanteDAL.ObterFabricanteClassificadoPorNome();
        }
    }
}
